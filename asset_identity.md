---
layout: page
title: Asset Identity und Asset Tracking
permalink: /asset_identity/
hide_from_menu: true
---

Dieser Use Case beschäftigt sich mit der Entwicklung von vertrauenswürdigen, Blockchain-basierten Identitäten für Assets, wie einzelne Fahrzeugkomponenten (z. B. Bremsen, einzelne Sensoren, etc.), dem Zug als solchen (Triebfahrzeug inkl. Waggons) oder der Infrastruktur (inkl. Betriebszentrale), um die Vertrauenswürdigkeit in das entsprechende Asset zu gewährleisten.

Zusätzlich sollen die Assets mit digitalen Lebensläufen (Asset Tracking) ausgerüstet werden, um eine bessere Nachvollziehbarkeit des Zustands des Assets zu ermöglichen (z. B. wenn ein Sensor von einem Zug 1 während der Instandhaltung zur Reparatur ausgebaut und in Zug 2 wieder montiert wurde).
Dieser Use Case ist Grundlage für die beiden weiteren Use Cases und das Vorhaben.
